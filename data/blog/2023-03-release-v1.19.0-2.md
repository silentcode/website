---
title: Forgejo v1.19 is available
publishDate: 2023-03-21
tags: ['releases']
excerpt: Forgejo v1.19 is available and comes with an extensive documentation, an experimental CI, incoming emails, scoped access tokens, registries for Cargo, Chef & Conda and more.
---

[Forgejo v1.19.0-2](https://codeberg.org/forgejo/forgejo/releases/tag/v1.19.0-2)
was released. It comes with an extensive [user guide](https://forgejo.org/docs/v1.19/user/), derived for the most part from the [Codeberg documentation](https://docs.codeberg.org/). The new [admin guide](https://forgejo.org/docs/v1.19/admin/) covers upgrades and configuration of Forgejo instances.

The most prominent new features are:

- **[Actions](https://forgejo.org/2023-02-27-forgejo-actions/)**: an experimental CI/CD, although not ready for real world usage, is present and [can be used to run a demo](https://forgejo.org/2023-02-27-forgejo-actions/).
- **[Incoming emails](https://forgejo.org/docs/v1.19/admin/incoming-email/)**: you can now set up Forgejo to receive incoming emails. When enabled, it is possible to reply to an email notification from Forgejo and (i) add a comment to an issue or a pull request, (ii) unsubscribe to notifications.
- **[Scoped access tokens](https://forgejo.org/docs/v1.19/user/oauth2-provider/#scoped-tokens)**: Forgejo access token, used with the [API](https://forgejo.org/docs/v1.19/user/api-usage/), can now have a "scope" that limits what it can access.
- **[Package registries](https://forgejo.org/docs/v1.19/user/packages/)** now support [Cargo](https://forgejo.org/docs/v1.19/user/packages/cargo/), [Conda](https://forgejo.org/docs/v1.19/user/packages/conda/) and [Chef](https://forgejo.org/docs/v1.19/user/packages/chef/).

Read more [in the Forgejo v1.19.0-2 release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-19-0-2)

### Get Forgejo v1.19

See the [download page](/download)
for instructions on how to install Forgejo, and read the
[release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-19-0-2)
for more information.

### Upgrading

Carefully read
[the breaking changes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-19-0-2)
section of the release notes.

The actual upgrade process is as simple as replacing the binary or container image
with the corresponding [Forgejo binary](https://codeberg.org/forgejo/forgejo/releases/tag/v1.19.0-2)
or [container image](https://codeberg.org/forgejo/-/packages/container/forgejo/1.19.0-2).
If you're using the container images, you can use the
[`1.19` tag](https://codeberg.org/forgejo/-/packages/container/forgejo/1.19)
to stay up to date with the latest `1.19.x` point release automatically.

Make sure to check the [Forgejo upgrade
documentation](https://forgejo.org/docs/v1.19/admin/upgrade/) for
recommendations on how to properly backup your instance before the
upgrade. It also covers upgrading from Gitea, as far back as version 1.2.0.
Forgejo includes all of Gitea v1.19, with improvements.

The Forgejo instances [built from an incorrect
tag](https://forgejo.org/2023-02-12-tags/) can safely be upgraded.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo, we'd love to hear from you!
Open an issue on [our issue tracker](https://codeberg.org/forgejo/forgejo/issues)
for feature requests or bug reports, find us [on the Fediverse](https://floss.social/@forgejo),
or drop into [our Matrix space](https://matrix.to/#/#forgejo:matrix.org)
([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) and say hi!
