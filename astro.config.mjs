import path from 'path';
import { fileURLToPath } from 'url';

import { defineConfig } from 'astro/config';

import tailwind from '@astrojs/tailwind';
import sitemap from '@astrojs/sitemap';
import image from '@astrojs/image';
import mdx from '@astrojs/mdx';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeSlug from 'rehype-slug';
import remarkGfm from 'remark-gfm';
import remarkSmartypants from 'remark-smartypants';

import { SITE } from './src/config.mjs';

const __dirname = path.dirname(fileURLToPath(import.meta.url));

function getAutolink(el) {
	const heading = el.children.length > 0 ? el.children[0].value : 'this';

	return {
		type: 'element',
		tagName: 'span',
		properties: {
			ariaLabel: `Permalink to „${heading}” section`,
			className: ['icon', 'icon-link'],
		},
		children: [],
	};
}

export default defineConfig({
	site: SITE.origin,
	base: SITE.basePathname,
	trailingSlash: SITE.trailingSlash ? 'always' : 'never',

	output: 'static',

	integrations: [
		tailwind({
			config: {
				applyBaseStyles: false,
			},
		}),
		sitemap(),
		image({
			serviceEntryPoint: '@astrojs/image/sharp',
		}),
		mdx(),
	],

	markdown: {
		rehypePlugins: [
			rehypeSlug,
			[
				rehypeAutolinkHeadings,
				{
					behavior: 'append',
					properties: {},
					content: getAutolink,
				},
			],
		],

		// GfM and Smartypants plugins can be dropped after upgrade to Astro v2:
		// https://docs.astro.build/en/guides/upgrade-to/v2/#changed-markdown-plugin-configuration
		remarkPlugins: [remarkGfm, remarkSmartypants],
	},

	vite: {
		resolve: {
			alias: {
				'~': path.resolve(__dirname, './src'),
			},
		},
	},
});
