---
layout: '~/layouts/Markdown.astro'
title: Code of Conduct and moderation team
license: 'CC-BY-SA-4.0'
---

Forgejo strives to be an inclusive project where everyone can participate in a safe environment. If a Forgejo community member feels unsafe for any reason, the **Moderation team** is available at `moderation@forgejo.org` and will put a stop to actions that are contrary to the [Code of Conduct](https://codeberg.org/forgejo/code-of-conduct) or the law.
