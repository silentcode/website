---
layout: '~/layouts/Markdown.astro'
title: 'License'
---

This documentation is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en).

The copyright holders are listed in the git history.

---

Some content is copied from https://github.com/go-gitea/gitea, published under the [Apache 2.0](https://github.com/go-gitea/gitea/blob/ffce336f1802b2f2298fc8fd27e815086702c812/docs/LICENSE) license.

---

Some of the content is copied from https://codeberg.org/Codeberg/Documentation/

The Codeberg logos in this website are by @mray,
licensed under [CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/).

The fonts in this website are documented on the [Codeberg Design repo](https://codeberg.org/Codeberg/Design#font-inter).

The icons in this website are by [Font Awesome](https://github.com/FortAwesome/Font-Awesome),
licensed under [multiple licenses](https://fontawesome.com/license/free).

Codeberg and the Codeberg Logo are trademarks of Codeberg e.V.

"Knut the Polar Bear" has been derived from https://openclipart.org/detail/193243/polar-bear-remix, under CC0 1.0
