---
layout: '~/layouts/Markdown.astro'
title: 'Package Registry'
---

## Supported package managers

The following package managers are currently supported:

| Name                   | Language   | Package client             |
| ---------------------- | ---------- | -------------------------- |
| [Cargo](cargo)         | Rust       | `cargo`                    |
| [Chef](chef)           | -          | `knife`                    |
| [Composer](composer)   | PHP        | `composer`                 |
| [Conan](conan)         | C++        | `conan`                    |
| [Conda](conda)         | -          | `conda`                    |
| [Container](container) | -          | any OCI compliant client   |
| [Generic](generic)     | -          | any HTTP client            |
| [Helm](helm)           | -          | any HTTP client, `cm-push` |
| [Maven](maven)         | Java       | `mvn`, `gradle`            |
| [npm](npm)             | JavaScript | `npm`, `yarn`, `pnpm`      |
| [NuGet](nuget)         | .NET       | `nuget`                    |
| [Pub](pub)             | Dart       | `dart`, `flutter`          |
| [PyPI](pypi)           | Python     | `pip`, `twine`             |
| [RubyGems](rubygems)   | Ruby       | `gem`, `Bundler`           |
| [Vagrant](vagrant)     | -          | `vagrant`                  |

**The following paragraphs only apply if Packages are not globally disabled!**

## Repository-Packages

A package always belongs to an owner (a user or organisation), not a repository.
To link an (already uploaded) package to a repository, open the settings page
on that package and choose a repository to link this package to.
The entire package will be linked, not just a single version.

Linking a package results in showing that package in the repository's package list,
and shows a link to the repository on the package site (as well as a link to the repository issues).

## Access Restrictions

| Package owner type | User                                                        | Organization                                             |
| ------------------ | ----------------------------------------------------------- | -------------------------------------------------------- |
| **read** access    | public, if user is public too; otherwise for this user only | public, if org is public, otherwise for org members only |
| **write** access   | owner only                                                  | org members with admin or write access to the org        |

N.B.: These access restrictions are subject to change, where more finegrained control will be added via a dedicated organization team permission.

## Create or upload a package

Depending on the type of package, use the respective package-manager for that. Check out the sub-page of a specific package manager for instructions.

## View packages

You can view the packages of a repository on the repository page.

1. Go to the repository.
1. Go to **Packages** in the navigation bar.

To view more details about a package, select the name of the package.

## Download a package

To download a package from your repository:

1. Go to **Packages** in the navigation bar.
1. Select the name of the package to view the details.
1. In the **Assets** section, select the name of the package file you want to download.

## Delete a package

You cannot edit a package after you have published it in the Package Registry. Instead, you
must delete and recreate it.

To delete a package from your repository:

1. Go to **Packages** in the navigation bar.
1. Select the name of the package to view the details.
1. Click **Delete package** to permanently delete the package.

## Disable the Package Registry

The Package Registry is automatically enabled. To disable it for a single repository:

1. Go to **Settings** in the navigation bar.
1. Disable **Enable Repository Packages Registry**.

Previously published packages are not deleted by disabling the Package Registry.
